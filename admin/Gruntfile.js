module.exports = function (grunt) {

	// Load all grunt tasks.
	require("jit-grunt")(grunt);

	grunt.initConfig({
		appDir: "./app",
		jsDir:  "<%- appDir %>/src",
		cssDir: "<%- appDir %>/assets/css",

		distDir: "<%= appDir %>/dist",

		concat: {
			app: {
				src: ["<%= jsDir %>/app.module.js", "<%= jsDir %>/app.route.js"],
				dest: "<%= distDir %>/app.js"
			},
			services: {
				src: ["<%= jsDir %>/**/*Service.js"],
				dest: "<%= distDir %>/app.services.js"
			},
			directives: {
				src: ["<%= jsDir %>/**/*Directive.js"],
				dest: "<%= distDir %>/app.directives.js"
			},
			filters: {
				src: ["<%= jsDir %>/**/*Filter.js"],
				dest: "<%= distDir %>/app.filters.js"
			},
			controllers: {
				src: ["<%= jsDir %>/**/*Controller.js"],
				dest: "<%= distDir %>/app.controllers.js"
			},
			models: {
				src: ["<%= jsDir %>/**/*Model.js"],
				dest: "<%= distDir %>/app.models.js"
			}
		},

		connect: {
			server: {
		      	options: {
		        	port: 9001,
		        	base: "app"
		      	}
		    }
		},

		concurrent: {
			debug: {
		        tasks: ["watch:js", "watch:livereload"],
		        options: {
		            logConcurrentOutput: true
		        }
		    }
		},

		watch: {
			js: {
				files: ["<%= jsDir %>/**/*.js"],
				tasks: ["build:js"]
			},
			livereload: {
				options: {
					livereload: true,
					port: 35729
				},
				files: ["<%= distDir %>/**/*.js", "<%= appDir %>/**/*.html"]
			}
		}

	});

	grunt.registerTask("build:js", ["concat"]);

	grunt.registerTask("default", ["build:js", "connect:server", "concurrent"]);
};