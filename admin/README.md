# Satchel Admin App

To run this app locally you need to follow these steps:

**You only need to do this once**

 - Clone this repository
 - Navigate your terminal to the repository
 - Run `npm install` command
 - Run `bower install` command

 - Once everything is installed run this command `grunt`