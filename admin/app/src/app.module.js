(function () {
	angular.module("satchel", ["ui.router", "ui.bootstrap"])
        .constant("satchel.Settings", {
            COMPONENTS_PATH: "/src/components",
            SHARED_PATH: "/src/shared"
        });
})();