(function () {
	angular.module("satchel")
		.controller("satchel.EditClientController", ["$state", "satchel.ClientsService", "satchel.ClientModel", editClientController]);

	function editClientController ($state, ClientsService, ClientModel) {
		var self = this;

		this.Client = {};

		this.updateClientDetails = function (clientId, clientDetails) {
			ClientsService.updateClientDetails($state.params.id).then(function (result) { 
				alert(result.message);
			});
		};

		ClientsService.getClientDetails($state.params.id).then(function (result) { 
			self.Client = new ClientModel(result);
		});
	}
})(); 