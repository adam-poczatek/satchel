(function () {
	angular.module("satchel")
		.controller("satchel.ClientManagerController", ["satchel.ClientsService", "satchel.ClientManagerModel", clientManagerController]);

	function clientManagerController (ClientsService, ClientManagerModel) {
		var self = this,
			clientManager = new ClientManagerModel();

		self.data = [];

		ClientsService.getAllClients().then(function (result) { 
			self.data = clientManager.addClients(result);
		});
	}
})(); 