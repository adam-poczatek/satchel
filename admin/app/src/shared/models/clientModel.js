(function () {
	angular.module("satchel")
		.factory("satchel.ClientModel", [function () {
			return Client;
		}]);

	function Client (data) {
		this.name = data.name;
		this.id = data.id;
	}
})();