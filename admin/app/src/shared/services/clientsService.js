(function () {
	angular.module("satchel")
		.factory("satchel.ClientsService", ["$http", function ($http, ClientManagerModel) {
			var ClientManagerService = {};

			ClientManagerService.getAllClients = function () {
				return $http.jsonp("https://angularjs.org/greet.php?callback=JSON_CALLBACK&name=Super%20Hero", {})
					.then(function (response) {
						var data = [{
							name: "Adam Poczatek",
							id: 09520948104124
						}, {
							name: "Aditya Arisetty",
							id: 59520948185021
						}];
						
						return data;
					});
			};

			ClientManagerService.getClientDetails = function (clientId) {
				return $http.jsonp("https://angularjs.org/greet.php?callback=JSON_CALLBACK&name=Super%20Hero", {})
					.then(function (response) {
						var data = {
							09520948104124: {
								name: "Adam Poczatek",
								id: 09520948104124
							},
							59520948185021: {
								name: "Aditya Arisetty",
								id: 59520948185021
							}
						};
						
						return data[clientId];
					});
			};

			ClientManagerService.updateClientDetails = function (clientId, clientDetails) {
				return $http.jsonp("https://angularjs.org/greet.php?callback=JSON_CALLBACK&name=Super%20Hero", {})
					.then(function (response) {
						return {
							message: "Client details updated!"
						}
					});
			}; 

			return ClientManagerService;
		}])

})();