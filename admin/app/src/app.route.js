(function () {
	angular.module("satchel")
		.config(["$stateProvider", "$urlRouterProvider", "satchel.Settings", function($stateProvider, $urlRouterProvider, Settings) {
			
			$urlRouterProvider.otherwise("/");
			
			$stateProvider
				.state("app", {
					url: "/",
					templateUrl: Settings.COMPONENTS_PATH + "/home/homeView.html",
					controller: "satchel.HomeController",
					controllerAs: "Home"
				})
				.state("clientManager", {
					url: "/client-manager",
					templateUrl: Settings.COMPONENTS_PATH + "/clientManager/clientManagerView.html",
					controller: "satchel.ClientManagerController",
					controllerAs: "ClientManager"
				})
				.state("editClient", {
					url: "/client-manager/:id",
					templateUrl: Settings.COMPONENTS_PATH + "/editClient/editClientView.html",
					controller: "satchel.EditClientController",
					controllerAs: "EditClient"
				});
			}]);
})();