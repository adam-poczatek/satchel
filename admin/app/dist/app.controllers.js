(function () {
	angular.module("satchel")
		.controller("satchel.ClientManagerController", ["satchel.ClientsService", "satchel.ClientManagerModel", clientManagerController]);

	function clientManagerController (ClientsService, ClientManagerModel) {
		var self = this,
			clientManager = new ClientManagerModel();

		self.data = [];

		ClientsService.getAllClients().then(function (result) { 
			self.data = clientManager.addClients(result);
		});
	}
})(); 
(function () {
	angular.module("satchel")
		.controller("satchel.EditClientController", ["$state", "satchel.ClientsService", "satchel.ClientModel", editClientController]);

	function editClientController ($state, ClientsService, ClientModel) {
		var self = this;

		this.Client = {};

		this.updateClientDetails = function (clientId, clientDetails) {
			ClientsService.updateClientDetails($state.params.id).then(function (result) { 
				alert(result.message);
			});
		};

		ClientsService.getClientDetails($state.params.id).then(function (result) { 
			self.Client = new ClientModel(result);
		});
	}
})(); 
(function () {
	angular.module("satchel")
		.controller("satchel.HomeController", ["$scope", homeController]);

	function homeController ($scope) {
		 
	}
})();