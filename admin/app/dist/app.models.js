(function () {
	angular.module("satchel")
		.factory("satchel.ClientManagerModel", ["satchel.ClientModel", clientManager]);

	function clientManager (Client) {
		var ClientManager = function () {
			this.data = [];
		};

		ClientManager.prototype.addClients = function (data) {
			var self = this;

			if (!Array.isArray(data)) {
				throw new Error("`data` is not an array.");
			}

			data.forEach(function (item) {
				self.addClient(item);
			});

			return this.data;
		}; 

		ClientManager.prototype.addClient = function (data) {
			var client = new Client(data);

			this.data[this.data.length] = client;

			return client;
		};

		ClientManager.prototype.removeClient = function (client) {
			var index = this.data.indexOf(client);

			if (index === -1) {
				throw new Error("Client not found.");
			}

			return this.data.splice(index, 1);
		};

		return ClientManager;
	}
})();
(function () {
	angular.module("satchel")
		.factory("satchel.ClientModel", [function () {
			return Client;
		}]);

	function Client (data) {
		this.name = data.name;
		this.id = data.id;
	}
})();