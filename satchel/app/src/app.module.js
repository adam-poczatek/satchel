(function () {
	"use strict";

	angular.module("satchel", ["ui.router", "ngTouch"])
        .constant("satchel.Settings", {
            COMPONENTS_PATH: "/src/components",
            SHARED_PATH: "/src/shared"
        });
})();