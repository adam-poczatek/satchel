(function () {
	"use strict";
	
	angular.module("satchel")
		.controller("satchel.ManageQuotationsController", ["$scope", ManageQuotationsController]);

	function ManageQuotationsController ($scope) {
		$scope.myCtrl = true;
	}
})();