(function () {
	"use strict";
	
	angular.module("satchel")
		.controller("satchel.UIController", ["$scope", UiController]);

	function UiController ($scope) {
		$scope.myCtrl = true;
	}
})();