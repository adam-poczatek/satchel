(function () {
	"use strict";
	
	angular.module("satchel")
		.controller("satchel.ContactController", ["$scope", ContactController]);

	function ContactController ($scope) {
		$scope.myCtrl = true;
	}
})();