(function () {
	"use strict";
	
	angular.module("satchel")
		.controller("satchel.ProfileController", ["$scope", ProfileController]);

	function ProfileController ($scope) {
		$scope.myCtrl = true;
	}
})();