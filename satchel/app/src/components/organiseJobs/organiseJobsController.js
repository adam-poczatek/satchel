(function () {
	"use strict";
	
	angular.module("satchel")
		.controller("satchel.OrganiseJobsController", ["$scope", OrganiseJobsController]);

	function OrganiseJobsController () {
		this.ctrl = true;
	}
})();