(function () {
	"use strict";
	
	angular.module("satchel")
		.controller("satchel.HomeController", ["$scope", homeController]);

	function homeController ($scope) {
		$scope.myCtrl = true;
	}
})();