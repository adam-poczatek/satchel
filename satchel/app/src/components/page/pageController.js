(function () {
	"use strict";
	
	angular.module("satchel")
		.controller("satchel.PageController", ["$rootScope", "$timeout", PageController]);

	function PageController ($rootScope, $timeout) {
		var self = this;

		this.toggleNavigation = function () {
			this.navigationVisible = !this.navigationVisible;
		};

		this.openNavigation = function () {
			this.navigationVisible = true;
		};

		this.closeNavigation = function () {
			this.navigationVisible = false;	
		};

		this.updateState = function () {
			$timeout(function () {
				self.closeNavigation();
			}, 400);
		};

		angular.element(document).ready(function () {
	        angular.element(document.body).removeClass("loading");
	    });

	    $rootScope.$on("$stateChangeSuccess", function (event, toState) {
	    	self.breadcrumb = toState.options.breadcrumb;
	    });
	}
})();