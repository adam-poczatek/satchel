(function () {
	"use strict";
	
	angular.module("satchel")
		.controller("satchel.ManageClientsController", ["$scope", ManageClientsController]);

	function ManageClientsController ($scope) {
		$scope.myCtrl = true;
	}
})();