(function () {
	"use strict";
	
	angular.module("satchel")
		.controller("satchel.EmployeesBookController", ["$scope", EmployeesBookController]);

	function EmployeesBookController ($scope) {
		$scope.myCtrl = true;
	}
})();