(function () {
	"use strict";
	
	angular.module("satchel")
		.controller("satchel.ProductsAndServicesController", ["$scope", ProductsAndServicesController]);

	function ProductsAndServicesController ($scope) {
		$scope.myCtrl = true;
	}
})();