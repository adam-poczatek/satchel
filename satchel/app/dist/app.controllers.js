(function () {
	"use strict";
	
	angular.module("satchel")
		.controller("satchel.ContactController", ["$scope", ContactController]);

	function ContactController ($scope) {
		$scope.myCtrl = true;
	}
})();
(function () {
	"use strict";
	
	angular.module("satchel")
		.controller("satchel.EmployeesBookController", ["$scope", EmployeesBookController]);

	function EmployeesBookController ($scope) {
		$scope.myCtrl = true;
	}
})();
(function () {
	"use strict";
	
	angular.module("satchel")
		.controller("satchel.HomeController", ["$scope", homeController]);

	function homeController ($scope) {
		$scope.myCtrl = true;
	}
})();
(function () {
	"use strict";
	
	angular.module("satchel")
		.controller("satchel.ManageClientsController", ["$scope", ManageClientsController]);

	function ManageClientsController ($scope) {
		$scope.myCtrl = true;
	}
})();
(function () {
	"use strict";
	
	angular.module("satchel")
		.controller("satchel.ManageQuotationsController", ["$scope", ManageQuotationsController]);

	function ManageQuotationsController ($scope) {
		$scope.myCtrl = true;
	}
})();
(function () {
	"use strict";
	
	angular.module("satchel")
		.controller("satchel.OrganiseJobsController", ["$scope", OrganiseJobsController]);

	function OrganiseJobsController () {
		this.ctrl = true;
	}
})();
(function () {
	"use strict";
	
	angular.module("satchel")
		.controller("satchel.PageController", ["$rootScope", "$timeout", PageController]);

	function PageController ($rootScope, $timeout) {
		var self = this;

		this.toggleNavigation = function () {
			this.navigationVisible = !this.navigationVisible;
		};

		this.openNavigation = function () {
			this.navigationVisible = true;
		};

		this.closeNavigation = function () {
			this.navigationVisible = false;	
		};

		this.updateState = function () {
			$timeout(function () {
				self.closeNavigation();
			}, 400);
		};

		angular.element(document).ready(function () {
	        angular.element(document.body).removeClass("loading");
	    });

	    $rootScope.$on("$stateChangeSuccess", function (event, toState) {
	    	self.breadcrumb = toState.options.breadcrumb;
	    });
	}
})();
(function () {
	"use strict";
	
	angular.module("satchel")
		.controller("satchel.ProductsAndServicesController", ["$scope", ProductsAndServicesController]);

	function ProductsAndServicesController ($scope) {
		$scope.myCtrl = true;
	}
})();
(function () {
	"use strict";
	
	angular.module("satchel")
		.controller("satchel.ProfileController", ["$scope", ProfileController]);

	function ProfileController ($scope) {
		$scope.myCtrl = true;
	}
})();
(function () {
	"use strict";
	
	angular.module("satchel")
		.controller("satchel.UIController", ["$scope", UiController]);

	function UiController ($scope) {
		$scope.myCtrl = true;
	}
})();