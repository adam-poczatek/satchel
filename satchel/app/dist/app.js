(function () {
	"use strict";

	angular.module("satchel", ["ui.router", "ngTouch"])
        .constant("satchel.Settings", {
            COMPONENTS_PATH: "/src/components",
            SHARED_PATH: "/src/shared"
        });
})();
(function () {
	"use strict";
	
	angular.module("satchel")
		.config(["$stateProvider", "$urlRouterProvider", "satchel.Settings", function($stateProvider, $urlRouterProvider, Settings) {
			
			$urlRouterProvider.otherwise("/");
			
			$stateProvider
				.state("home", {
					url: "/",
					templateUrl: Settings.COMPONENTS_PATH + "/home/homeView.html",
					controller: "satchel.HomeController",
					controllerAs: "Home",
					options: {
						breadcrumb: [""]
					}
				})
				.state("profile", {
					url: "/profile",
					templateUrl: Settings.COMPONENTS_PATH + "/profile/profileView.html",
					controller: "satchel.ProfileController",
					controllerAs: "Profile",
					options: {
						breadcrumb: ["Profile"]
					}
				})
				.state("manage-quotations", {
					url: "/manage-quotations",
					templateUrl: Settings.COMPONENTS_PATH + "/manageQuotations/manageQuotationsView.html",
					controller: "satchel.ManageQuotationsController",
					controllerAs: "ManageQuotations",
					options: {
						breadcrumb: ["Manage Quotations"]
					}
				})
				.state("employees-book", {
					url: "/employees-book",
					templateUrl: Settings.COMPONENTS_PATH + "/employeesBook/employeesBookView.html",
					controller: "satchel.EmployeesBookController",
					controllerAs: "EmployeesBook",
					options: {
						breadcrumb: ["Employees Book"]
					}
				})
				.state("manage-clients", {
					url: "/manage-clients",
					templateUrl: Settings.COMPONENTS_PATH + "/manageClients/manageClientsView.html",
					controller: "satchel.ManageClientsController",
					controllerAs: "ManageClients",
					options: {
						breadcrumb: ["Manage Client"]
					}
				})
				.state("organise-jobs", {
					url: "/organise-jobs",
					templateUrl: Settings.COMPONENTS_PATH + "/organiseJobs/organiseJobsView.html",
					controller: "satchel.OrganiseJobsController",
					controllerAs: "OrganiseJobs",
					options: {
						breadcrumb: ["Organise Jobs"]
					}
				})
				.state("products-and-services", {
					url: "/products-and-services",
					templateUrl: Settings.COMPONENTS_PATH + "/productsAndServices/productsAndServicesView.html",
					controller: "satchel.ProductsAndServicesController",
					controllerAs: "ProductsAndServices",
					options: {
						breadcrumb: ["Products & Services"]
					}
				})
				.state("contact", {
					url: "/contact",
					templateUrl: Settings.COMPONENTS_PATH + "/contact/contactView.html",
					controller: "satchel.ContactController",
					controllerAs: "Contact",
					options: {
						breadcrumb: ["Contact"]
					}
				})

				.state("ui", {
					url: "/ui",
					templateUrl: Settings.COMPONENTS_PATH + "/ui/uiView.html",
					controller: "satchel.UIController",
					controllerAs: "UI",
					options: {
						breadcrumb: ["UI", "HTML"]
					}
				});
			}]);
})();