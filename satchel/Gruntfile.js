module.exports = function (grunt) {

	// Load all grunt tasks.
	require("jit-grunt")(grunt);

	grunt.initConfig({
		appDir: "./app",
		jsDir:  "<%- appDir %>/src",
		cssDir: "<%- appDir %>/assets/css",

		distDir: "<%= appDir %>/dist",

		// Server

		connect: {
			server: {
		      	options: {
		        	port: 9001,
		        	base: "app"
		      	}
		    }
		},

		// JS

		concat: {
			app: {
				src: ["<%= jsDir %>/app.module.js", "<%= jsDir %>/app.route.js"],
				dest: "<%= distDir %>/app.js"
			},
			services: {
				src: ["<%= jsDir %>/**/*Service.js"],
				dest: "<%= distDir %>/app.services.js"
			},
			directives: {
				src: ["<%= jsDir %>/**/*Directive.js"],
				dest: "<%= distDir %>/app.directives.js"
			},
			filters: {
				src: ["<%= jsDir %>/**/*Filter.js"],
				dest: "<%= distDir %>/app.filters.js"
			},
			controllers: {
				src: ["<%= jsDir %>/**/*Controller.js"],
				dest: "<%= distDir %>/app.controllers.js"
			},
			models: {
				src: ["<%= jsDir %>/**/*Model.js"],
				dest: "<%= distDir %>/app.models.js"
			}
		},

		jshint: {
			options: {
                jshintrc: true
		    },
		    app: {
		    	files: {
		    		src: [
		    			"<%= jsDir %>/**/*.js",
		    			"!<%= distDir %>/app.js",
						"!<%= distDir %>/app.services.js",
						"!<%= distDir %>/app.directives.js",
						"!<%= distDir %>/app.filters.js",
						"!<%= distDir %>/app.controllers.js",
						"!<%= distDir %>/app.models.js"
	    			]
		    	}
		    }
		},

		karma: {
			app: {
				configFile: "./karma.conf.js",
				singleRun: true
			}
		},

		// CSS

		autoprefixer: {
			options: {
				browsers: ["last 2 versions", "ie 9", "ie 10"]
			},
			app: {
				src: "<%= cssDir %>/style.raw.css",
				dest: "<%= cssDir %>/style.css"
			}
		},

		sass: {
			app: {
		        files: {
		            "<%= cssDir %>/style.raw.css": "<%= cssDir %>/style.scss"
		        }
		    }
		},

		// Build

		watch: {
			autoprefixer: {
				files: ["<%= cssDir %>/style.raw.css"],
				tasks: ["autoprefixer"]
			},
			sass: {
				files: ["<%= cssDir %>/**/*.scss"],
				tasks: ["sass"]
			},
			js: {
				files: ["<%= jsDir %>/**/*.js"],
				tasks: ["build:js", "jshint:app"]
			},
			livereload: {
				options: {
					livereload: true,
					port: 35729
				},
				files: ["<%= cssDir %>/style.css", "<%= distDir %>/**/*.js", "<%= appDir %>/**/*.html"]
			}
		},

		concurrent: {
			debug: {
		        tasks: ["watch:sass", "watch:autoprefixer", "watch:js", "watch:livereload"],
		        options: {
		            logConcurrentOutput: true
		        }
		    }
		}

	});

	grunt.registerTask("build:js", ["concat"]);

	grunt.registerTask("default", ["build:js", "connect:server", "concurrent"]);
};