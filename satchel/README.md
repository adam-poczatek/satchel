# Installation

Firstly, install [Node.js](http://nodejs.org/) if you don't have it.

If you're on a Windows machine you need to make sure you have ruby installed. Also, go for 1.9.x version as we have 
seen some problems with newer versions. [Get Ruby 1.9.3 Windows installer](http://dl.bintray.com/oneclick/rubyinstaller/rubyinstaller-1.9.3-p551.exe?direct).

**Installing Sass**

Install Ruby using the downloaded file and in the console run `gem install sass`.

**Installing Grunt**

Open console and run `npm install grunt -g` and `npm install grunt-cli -g`.

**Installing Bower**

Open console and run `npm install bower -g`.

**Install Project**

 1. Clone this repository
 2. Navigate your terminal to the repository
 3. Run `npm install` command
 4. Run `bower install` command

 - Once everything is installed run `grunt` command. If everything is correct you should be able to access the site
 at [http://localhost:9001](http://localhost:9001).